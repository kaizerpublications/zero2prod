mod health_check;
mod helpers;
mod subscriptions;

// New module!
mod subscriptions_confirm;

// New test module!
mod change_password;
mod login;
mod newsletter;

mod admin_dashboard;

fn main() {
    println!("Hello, world!");
}
